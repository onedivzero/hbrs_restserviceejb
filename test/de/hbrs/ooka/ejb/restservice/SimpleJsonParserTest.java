/*
 * Note:      beans.xml had to been renamed in order to get tests running
 * @Error:    java.lang.IllegalAccessError: tried to access class javax.xml.bind.ContextFinder from class javax.xml.bind.ContextFinder$2
 * @SEE:      http://www.hameister.org/JEE6_EJBTests.html
 * @Error:    Doing it this way prevents CDI-Injection, when the EJBs are used by another project ! 
 * Solution:  Add properties with EJB-AppName to EJB-Container in 'setUpClass'-method and modify path in 'ctx.lookup()'-method
 * @SEE:      http://forums.netbeans.org/ptopic51295.html
 */

package de.hbrs.ooka.ejb.restservice;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michael Ajwani
 */
public class SimpleJsonParserTest {
  
  private static String EJB_APP_NAME = "OOKA_RestServiceEJB";
  
  private static Context ctx;
  private static EJBContainer Container;
  
  private static SimpleJsonParser SimpleParser;
  
  private static String username         = "apiuser";
  private static String password         = "api";  
  
  private String service_BaseURL          = "http://localhost:3000/";
  private String url_getAllCourses        = service_BaseURL + "courses.json";
  
  public SimpleJsonParserTest() {
  }
  
  @BeforeClass
  public static void setUpClass() throws NamingException {
    System.out.println("Opening the EJB-Container");
    
    Map<Object, Object> properties = new HashMap<>(); 
    properties.put(EJBContainer.APP_NAME, EJB_APP_NAME);

//    Another workaround for running all Junit-Tests in one call instead of each file standalone (DNW!)
//    @SEE: https://netbeans.org/bugzilla/show_bug.cgi?id=179355    
//    File[] ejbModules = new File[1];
//    ejbModules[0] = new File("dist/OOKA_RestServiceEJB.jar");
//    properties.put(EJBContainer.MODULES, ejbModules);    
    
    Container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    ctx       = Container.getContext();
    
    SimpleParser = (SimpleJsonParser) ctx.lookup("java:global/" + EJB_APP_NAME + "/classes/SimpleJsonParser");
    SimpleParser.setHttpCredentials(username, password);
  }
  
  @AfterClass
  public static void tearDownClass() {
    System.out.println("Closing the EJB-Container");
    Container.close();
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }


  /**
   * Test of setHttpCredentials method, of class SimpleJsonParser.
   */
  @Test
  public void testSetHttpCredentials() {
    System.out.println("setHttpCredentials");

    String expectedResult = "YXBpdXNlcjphcGk=";
    SimpleParser.setHttpCredentials(username, password);
    String Credentials = SimpleParser.getHttpCredentials();
    
    assertNotNull(Credentials);
    assertEquals("Credentials should be encoded properly", expectedResult, Credentials);
  }

  
  /**
   * Test of toJson method, of class SimpleJsonParser.
   */
  @Test
  public void testToJson() {
    System.out.println("toJson");
    
    Course TestCourse       = this.buildNewCourseObject();
    String ExpectedResult   = "{\"id\":0,\"created_at\":\"2012.10.28 01:30\",\"updated_at\":\"2012.10.29 01:30\",\"name\":\"JUnit-TestCourse from Object\",\"startdate\":\"2012-10-03\",\"starttime\":\"11:15\",\"enddate\":\"2013-01-26\",\"endtime\":\"15:45\",\"description\":\"Some description text for \\u0027JUnit-TestCourse from Object\\u0027\",\"credits\":6.5,\"exam\":\"2013-03-11 09:30\",\"participants\":15,\"rate\":1.5121558,\"lecturer_id\":2,\"flag\":false,\"test_stuff\":\"abc\"}";
    
    String ResultAsJSON = SimpleParser.toJson(TestCourse);

    assertNotNull(ResultAsJSON);
    assertEquals(ExpectedResult, ResultAsJSON);
  }
  
  
  /**
   * Test of toEntity method, of class SimpleJsonParser.
   * Note: This is rather a test for the Course-Class, since there are some date-conversions behind the scene
   * But: it must also ensured, that conversions from JSON are fine
   */
  @Test
  public void testToEntity() {
    System.out.println("toEntity");
    
    SimpleParser.setResultClassType(Course.class);
    
    String JsonContent  = "{\"id\":1,\"created_at\":\"2012.10.28 01:30\",\"updated_at\":\"2012.10.29 01:30\",\"name\":\"JUnit-TestCourse from Object\",\"startdate\":\"2012-10-03\",\"starttime\":\"11:15\",\"enddate\":\"2013-01-26\",\"endtime\":\"15:45\",\"description\":\"Some description text for \\u0027JUnit-TestCourse from Object\\u0027\",\"credits\":6.5,\"exam\":\"2013-03-11 09:30\",\"participants\":15,\"rate\":1.5121558,\"lecturer_id\":2,\"flag\":false,\"test_stuff\":\"abc\"}";
    Course expResult    = (Course) SimpleParser.toEntity(JsonContent);
    
    assertNotNull(expResult);
    
    // Assertions for common datatypes and dates represented as strings
    assertEquals(expResult.getId(), 1);
    assertEquals(expResult.getName(), "JUnit-TestCourse from Object");
    assertEquals(expResult.getDescription(), "Some description text for 'JUnit-TestCourse from Object'");
    assertEquals(expResult.getCredits(), 6.5, 1);
    assertEquals(expResult.getParticipants(), 15);
    assertEquals(expResult.getRate(), 1.5121558F, 7);
    assertEquals(expResult.getLecturer_id(), 2);
    assertEquals(expResult.isFlag(), false);                      // TODO: Method 'getFlag' does not exist in Course-Object!
    assertEquals(expResult.getCreated_at(), "2012.10.28 01:30");
    assertEquals(expResult.getUpdated_at(), "2012.10.29 01:30");
    assertEquals(expResult.getStarttime(), "11:15");
    assertEquals(expResult.getEndtime(), "15:45");
    assertEquals(expResult.getTest_stuff(), "abc");               // Assertion for non-existing property (is only ignored by rest-service)
       
    // Assertions for date-conversions made inside the Course-Class
    Date startDate  = expResult.getStartdate();
    Date endDate    = expResult.getEnddate();
    Date examDate   = expResult.getExam();
    
    Date expectedStartDate  = this.convertDate("2012-10-03", "yyyy-MM-dd");
    Date expectedEndDate    = this.convertDate("2013-01-26", "yyyy-MM-dd");
    Date expectedExamDate   = this.convertDate("2013-03-11 09:30".replace("T", " ").replace("Z", " "), "yyyy-MM-dd HH:mm:ss");
    
    assertNotNull(startDate);
    assertEquals(startDate, expectedStartDate);
    assertEquals(startDate.toString() , expectedStartDate.toString());

    assertNotNull(endDate);
    assertEquals(endDate, expectedEndDate);
    assertEquals(endDate.toString(), expectedEndDate.toString());
    
    // FIXME: 'String JsonContent' should be in format provided by Rest-Service
    // Failed to parse Date from JSON: 2013-03-11 09:30
    //assertNotNull(examDate);
    //assertEquals(examDate, expectedExamDate);
    //assertEquals(examDate.toString(), expectedExamDate.toString());
  }
  
  
  /**
   * Test of fetchLocalJsonContent method, of class SimpleJsonParser.
   * Note: No test for second method with ServletContext provided
   */
  @Test
  public void testFetchLocalJsonContent() {
    System.out.println("fetchLocalJsonContent");
  
    String localJsonSource  = "jsontestfile.json";
  
    SimpleParser.setResultClassType(TwitterUser.class);
    List<TwitterUser> result = SimpleParser.fetchLocalJsonContent(localJsonSource);

    assertNotNull(result);
    assertEquals(result.size(), 2);
  }


  /**
   * Test of fetchRemoteJsonContent method, of class SimpleJsonParser.
   */
  @Test
  public void testFetchRemoteJsonContent() {
    System.out.println("fetchRemoteJsonContent");
    
    SimpleParser.setHttpCredentials("apiuser", "api");
    SimpleParser.setResultClassType(Course.class);
    
    List<Course> result     = SimpleParser.fetchRemoteJsonContent(this.url_getAllCourses);        
    
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }
  

  /**
   * Test of fetchStringJsonContent method, of class SimpleJsonParser.
   */
  @Test
  public void testFetchStringJsonContent() {
    System.out.println("fetchStringJsonContent");
    
    String JsonContent  = "{\"id\":1,\"created_at\":\"2012.10.28 01:30\",\"updated_at\":\"2012.10.29 01:30\",\"name\":\"JUnit-TestCourse from Object\",\"startdate\":\"2012-10-03\",\"starttime\":\"11:15\",\"enddate\":\"2013-01-26\",\"endtime\":\"15:45\",\"description\":\"Some description text for \\u0027JUnit-TestCourse from Object\\u0027\",\"credits\":6.5,\"exam\":\"2013-03-11 09:30\",\"participants\":15,\"rate\":1.5121558,\"lecturer_id\":2,\"flag\":false,\"test_stuff\":\"abc\"}";
    
    SimpleParser.setResultClassType(Course.class);
    
    List<Course> result     = SimpleParser.fetchStringJsonContent(JsonContent);
    
    assertNotNull(result);
    assertTrue(result.get(0).getName().equals("JUnit-TestCourse from Object"));
  }

  
  /**
   * Private Test-Helper-Methods
   */    
  
  private Course buildNewCourseObject() {

    Course TestCourse = new Course();
    TestCourse.setName("JUnit-TestCourse from Object");
    TestCourse.setStartdate("2012-10-03");
    TestCourse.setStarttime("11:15");
    TestCourse.setEnddate("2013-01-26");
    TestCourse.setEndtime("15:45");
    TestCourse.setDescription("Some description text for 'JUnit-TestCourse from Object'");
    TestCourse.setCredits(6.5);
    TestCourse.setExam("2013-03-11 09:30");
    TestCourse.setParticipants(15);
    TestCourse.setRate(1.5121558F);
    TestCourse.setLecturer_id(2);
    TestCourse.setFlag(false);
    TestCourse.setCreated_at("2012.10.28 01:30"); // Will be ignored by remote-service, cause it's auto-generated
    TestCourse.setUpdated_at("2012.10.29 01:30"); // Will be ignored by remote-service, cause it's auto-generated
    TestCourse.setTest_stuff("abc");              // This attribute does not exist at remote-service, will just be ignored to our rescue !
    
    return TestCourse;
  }
  
  
  private Date convertDate(String DateValue, String Format) {
    try {
      return new SimpleDateFormat(Format).parse(DateValue);
    } catch (ParseException | NullPointerException ex) {
      Logger.getLogger(Course.class.getName()).log(Level.SEVERE, "Failed to parse Date from JSON: {0}", DateValue);
    }
    return null;    
  }

}
