package de.hbrs.ooka.ejb.restservice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Michael Ajwani
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({de.hbrs.ooka.ejb.restservice.RestServiceInterfaceTest.class, de.hbrs.ooka.ejb.restservice.SimpleJsonParserTest.class})
public class RestServiceComponentsTestSuite {

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
}
