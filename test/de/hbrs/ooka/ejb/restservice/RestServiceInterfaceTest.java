/*
 * Note:      beans.xml had to been renamed in order to get tests running
 * @Error:    java.lang.IllegalAccessError: tried to access class javax.xml.bind.ContextFinder from class javax.xml.bind.ContextFinder$2
 * @SEE:      http://www.hameister.org/JEE6_EJBTests.html
 * @Error:    Doing it this way prevents CDI-Injection, when the EJBs are used by another project ! 
 * Solution:  Add properties with EJB-AppName to EJB-Container in 'setUpClass'-method and modify path in 'ctx.lookup()'-method
 * @SEE:      http://forums.netbeans.org/ptopic51295.html
 */

package de.hbrs.ooka.ejb.restservice;

import com.sun.jersey.api.representation.Form;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michael Ajwani
 */
public class RestServiceInterfaceTest {
  
  private static String EJB_APP_NAME = "OOKA_RestServiceEJB";
  
  private static Context ctx;
  private static EJBContainer Container;
  
  private static RestServiceInterface RestService;
  private static SimpleJsonParser SimpleParser;
  
  private static String username         = "apiuser";
  private static String password         = "api";

  // Note: You should only test against localhost by default and not against the production-system
  // For testing remote you should setup a stage-environment
  
  //private String service_BaseURL          = "http://hbrs-rest-services.herokuapp.com/";
  private String service_BaseURL          = "http://localhost:3000/";
    
  private String url_getAllCourses        = service_BaseURL + "courses.json";
  private String url_getFirstCourse       = service_BaseURL + "courses/1.json";
  private String url_getCourseByID        = service_BaseURL + "courses/#ID#.json";
  private String url_createCourse         = service_BaseURL + "courses";
  private String url_updateCourse         = service_BaseURL + "courses/";  
  private String url_deleteCourse         = service_BaseURL + "courses/";
  private String url_getAllLecturers      = service_BaseURL + "lecturers.json";  

  
  public RestServiceInterfaceTest() {
  }
  
  
  @BeforeClass
  public static void setUpClass() throws NamingException {
    System.out.println("Opening the EJB-Container");
    
    Map<Object, Object> properties = new HashMap<>(); 
    properties.put(EJBContainer.APP_NAME, EJB_APP_NAME);
    
    Container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);
    ctx       = Container.getContext();
    
    RestService  = (RestServiceInterface) ctx.lookup("java:global/" + EJB_APP_NAME + "/classes/RestServiceInterface");
    RestService.setCredentials(username, password);
    
    SimpleParser = (SimpleJsonParser) ctx.lookup("java:global/" + EJB_APP_NAME + "/classes/SimpleJsonParser");
    SimpleParser.setHttpCredentials(username, password);
  }
  
  
  @AfterClass
  public static void tearDownClass() {
    System.out.println("Closing the EJB-Container");
    Container.close();
  }
  
  
  @Before
  public void setUp() {
  }
  
  
  @After
  public void tearDown() {
  }
  

  /**
   * testInitialState of class RestServiceInterface.
   */  
  @Test
  public void testInitialState() {
    RestService.initResource(this.url_getAllCourses);
    assertEquals("Inital ResponseState should be '0'", 0, RestService.getResponseState());
    assertEquals("Initial ResponseString should be empty", "", RestService.getResponseString());
  }

  
  /**
   * Test of getResource method, of class RestServiceInterface.
   * TODO: Deep-Check of content! RestService returns also '200 OK' if entity with specified ID is not present!
   * There should be an additional route at the rest-service for getting first and last course for
   * having specific entities used or generated by testing
   */
  @Test
  public void testGetResource() {
    System.out.println("getResource");

    RestService.initResource(this.url_getAllCourses);
    String GetResponse_AllCoursesAsJSON = RestService.getResource();
   
    assertNotNull(GetResponse_AllCoursesAsJSON);
    assertFalse(GetResponse_AllCoursesAsJSON.isEmpty());
    assertEquals("ResponseState should be 200", 200, RestService.getResponseState());

    RestService.initResource(this.url_getFirstCourse);
    String GetResponse_FirstCourseAsJSON = RestService.getResource();
   
    System.out.println(GetResponse_FirstCourseAsJSON);
    
    assertNotNull(GetResponse_FirstCourseAsJSON);
    assertFalse(GetResponse_FirstCourseAsJSON.isEmpty());
    assertEquals("ResponseState should be 200", 200, RestService.getResponseState());
  }

  
  /**
   * Test of postResource method of class RestServiceInterface using Object-to-JSON-Conversion
   */
  @Test
  public void testPostResource() {
    System.out.println("postResource");
        
    Course NewTestCourse = this.buildNewCourseObject();
        
    SimpleParser.setResultClassType(Course.class);  
    String TestCourseAsJSON = SimpleParser.toJson(NewTestCourse);
    assertNotNull(TestCourseAsJSON);
    assertFalse(TestCourseAsJSON.isEmpty());
    
    RestService.initResource(this.url_createCourse);
    
    String PostResponse_NewTestCourseAsJSON  = RestService.postResource(TestCourseAsJSON);
    assertNotNull(PostResponse_NewTestCourseAsJSON);
    assertFalse(PostResponse_NewTestCourseAsJSON.isEmpty());
    assertEquals("ResponseState should be 201", 201, RestService.getResponseState());
  }
  

  /**
   * Test of putResource method, of class RestServiceInterface.
   */
  @Test
  public void testPutResource() {
    System.out.println("putResource");
    
    Course ToBeUpdatedCourse = this.getRandomCourseFromService();
    assertNotNull(ToBeUpdatedCourse);
    
    ToBeUpdatedCourse.setName(ToBeUpdatedCourse.getName() + " Test-" + System.currentTimeMillis());
    String ToBeUpdatedCourseAsJSON = SimpleParser.toJson(ToBeUpdatedCourse);
    
    RestService.initResource(url_updateCourse + ToBeUpdatedCourse.getId());
    String PutResponse_ToBeUpdatedCourseAsJSON = RestService.putResource(ToBeUpdatedCourseAsJSON);        
    
    assertNotNull(PutResponse_ToBeUpdatedCourseAsJSON);
    assertFalse(PutResponse_ToBeUpdatedCourseAsJSON.isEmpty());
    assertEquals("ResponseState should be 204", 204, RestService.getResponseState());
  }
  
  
  /**
   * Test of deleteResource method, of class RestServiceInterface.
   */
  @Test
  public void testDeleteResource() {
    System.out.println("deleteResource");

    Course ToBeDeletedCourse = this.getRandomCourseFromService();
    assertNotNull(ToBeDeletedCourse);    
    
    RestService.initResource(url_deleteCourse + ToBeDeletedCourse.getId());
    String DeleteResponse_ToBeDeletedCourseAsJSON = RestService.deleteResource();
    
    assertNotNull(DeleteResponse_ToBeDeletedCourseAsJSON);
    assertFalse(DeleteResponse_ToBeDeletedCourseAsJSON.isEmpty());
    assertEquals("ResponseState should be 204", 204, RestService.getResponseState());    
  }
  

  /**
   * Test of getPostForm method, of class RestServiceInterface.
   */
  @Test
  public void testGetPostForm() {
    System.out.println("getPostForm");

    Form JerseyRepresentationForm = RestService.getPostForm();
    assertNotNull(JerseyRepresentationForm);
    assertTrue(JerseyRepresentationForm instanceof com.sun.jersey.api.representation.Form);
  }
  

  /**
   * Test of postResourceFormEncoded method of class RestServiceInterface using Form-Object
   */
  @Test
  public void testPostResourceFormEncoded() {
    System.out.println("postResourceFormEncoded");

    Form MultiPartPostForm = this.preparePostForm();  
    RestService.initResource(url_createCourse);
    
    String PostResponse_NewCourseAsJson = RestService.postResourceFormEncoded(MultiPartPostForm);

    assertNotNull(PostResponse_NewCourseAsJson);
    assertFalse(PostResponse_NewCourseAsJson.isEmpty());
    assertEquals("ResponseState should be 201", 201, RestService.getResponseState());    
  }

  /**
   * Private Test-Helper-Methods
   */  
  
  private Course getRandomCourseFromService() {
    System.out.println("getRandomCourseFromService");
    
    RestService.initResource(this.url_getAllCourses);
    
    String GetResponse_AllCoursesAsJSON   = RestService.getResource();
    assertNotNull(GetResponse_AllCoursesAsJSON);
    assertFalse(GetResponse_AllCoursesAsJSON.isEmpty());
    
    List<Course> CourseList = SimpleParser.fetchStringJsonContent(GetResponse_AllCoursesAsJSON);
    assertNotNull(CourseList);
    assertTrue(CourseList.size() > 0);
    
    int ExistingRandomId = CourseList.get(new java.util.Random().nextInt(CourseList.size())).getId();

    RestService.initResource(this.url_getCourseByID.replace("#ID#", String.valueOf(ExistingRandomId)));
    String GetResponse_RandomCourseAsJSON = RestService.getResource();
    assertNotNull(GetResponse_RandomCourseAsJSON);
    assertFalse(GetResponse_RandomCourseAsJSON.isEmpty());
    
    Course RandomCourse = (Course) SimpleParser.toEntity(GetResponse_RandomCourseAsJSON);
    assertNotNull(RandomCourse);
    
    return RandomCourse;
  }
    
  
  private Course buildNewCourseObject() {

    Course TestCourse = new Course();
    TestCourse.setName("JUnit-TestCourse from Object");
    TestCourse.setStartdate("2012-10-03");
    TestCourse.setStarttime("11:15");
    TestCourse.setEnddate("2013-01-26");
    TestCourse.setEndtime("15:45");
    TestCourse.setDescription("Some description text for 'JUnit-TestCourse from Object'");
    TestCourse.setCredits(6.5);
    TestCourse.setExam("2013-03-11 09:30");
    TestCourse.setParticipants(15);
    TestCourse.setRate(1.5121558F);
    TestCourse.setLecturer_id(2);
    TestCourse.setFlag(false);
    TestCourse.setCreated_at("2012.10.28 01:30"); // Will be ignored by remote-service, cause it's auto-generated
    TestCourse.setUpdated_at("2012.10.29 01:30"); // Will be ignored by remote-service, cause it's auto-generated
    TestCourse.setTest_stuff("abc");              // This attribute does not exist at remote-service, will just be ignored to our rescue !
    
    return TestCourse;
  }
  
  
  private Form preparePostForm() {

    // Field-names must hit Rails-conventions: '$singular_entity_name[$entitiy_attribute_name]'
    
    Form MultiPartPostForm = RestService.getPostForm();
    
    MultiPartPostForm.add("course[name]", "JUnit-TestCourse from PostForm");   
    MultiPartPostForm.add("course[startdate]", "2012-10-15");   
    MultiPartPostForm.add("course[starttime]", "09:30");
    MultiPartPostForm.add("course[enddate]", "2013-02-01");   
    MultiPartPostForm.add("course[endtime]", "12:00");    
    MultiPartPostForm.add("course[description]", "Some description text for 'JUnit-TestCourse from PostForm'");
    MultiPartPostForm.add("course[credits]", 5.5);    
    MultiPartPostForm.add("course[exam]", "2013-03-16 08:00");
    MultiPartPostForm.add("course[participants]", 16);
    MultiPartPostForm.add("course[rate]", 1.454158);
    MultiPartPostForm.add("course[lecturer_id]", 1);
    MultiPartPostForm.add("course[flag]", true);
    
    return MultiPartPostForm;
  }
  
}
