package de.hbrs.ooka.ejb.restservice;

/**
 *
 * @author Micha
 * @SEE: http://www.javabeat.net/2012/04/parsing-json-using-java-and-gson-library/
 */

/**
* Model class for storing the selected few attributes
* for a Twitter User profile.
*/

class TwitterUser {
  
  private String id;
  private String name;
  private String screen_name;
  private String url;
  private int friends_count;
  private int followers_count;
  private int favourites_count;
  private int statuses_count;
  
  public TwitterUser(){
  }
  
  @Override
  public String toString(){
    return "\n" 
      + "Name: "+this.name+"\n" 
      + "Screen Name: "+this.screen_name+"\n" 
      + "Number of Friends: "+ this.friends_count + "\n" 
      + "Number of Followers: "+ this.followers_count +"\n"
      + "Number of Status updates: "+this.statuses_count +"\n"
      + "Number of favorites: "+this.favourites_count +"\n";
  }
  
}
