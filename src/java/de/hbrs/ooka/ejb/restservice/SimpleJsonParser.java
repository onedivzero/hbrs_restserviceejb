package de.hbrs.ooka.ejb.restservice;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateful;
import javax.servlet.ServletContext;

/**
 *
 * @author Micha
 */

@Stateful
public class SimpleJsonParser<T> {
  
  private List<T> ResultList;
  private Class ResultClassType;
  
  private InputStreamReader ISR;
  private Gson gsonInstance;
  
  private JsonReader reader;
  private JsonParser jsonParser;
  private JsonElement resultElement;
  
  private JsonObject resultObject;
  private JsonArray resultArray;
  private String resultString;
  
  private String HttpCredentials = null;
  
  
  public SimpleJsonParser() {
    
    this.ISR             = null;
    this.gsonInstance    = new Gson();    
    this.jsonParser      = new JsonParser();
  }
  
  public SimpleJsonParser(Class ResultClassType) {
    
    this();
    this.ResultClassType = ResultClassType;
  }

// --------------------------------------------------------------------------------
// Methods for preparing SJP
// --------------------------------------------------------------------------------    
  
  public void setResultClassType(Class ResultClassType) {
    this.ResultClassType = ResultClassType;
  } 
  
  
  public void setHttpCredentials(String username, String password) {
    
    String RawCredentials = username + ":" + password;
    this.HttpCredentials  = new sun.misc.BASE64Encoder().encode(RawCredentials.getBytes());
  }
  
  // Added for testing
  public String getHttpCredentials() {
    return this.HttpCredentials;
  }

// --------------------------------------------------------------------------------
// Methods for converting from or to JsON
// --------------------------------------------------------------------------------  

  public String toJson(Object Entity) {
    
    return this.gsonInstance.toJson(Entity);
  }
  
  public Object toEntity(String JsonContent) {
    
    return gsonInstance.fromJson(JsonContent, this.ResultClassType);
  }
    
// --------------------------------------------------------------------------------
// Methods for fetching JSON-Content from local, remote or direct string resources
// --------------------------------------------------------------------------------
  
  public List<T> fetchLocalJsonContent(String SourcePath, ServletContext ctx) {
    
    return this.fetchLocalJsonContent(ctx.getRealPath(SourcePath));
  }
  
  public List<T> fetchLocalJsonContent(String realLocalSourcePathString) {
    
    try {
      
      this.ISR = new InputStreamReader(new FileInputStream(realLocalSourcePathString));
      
    } catch (FileNotFoundException ex) {
      Logger.getLogger(SimpleJsonParser.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return this.fetchJsonContent();
  }

  public List<T> fetchRemoteJsonContent(String remoteUrlString) {
    
    try {
      
      URLConnection urlConnection = new URL(remoteUrlString).openConnection();
      
      if(this.HttpCredentials != null) {
        urlConnection.setRequestProperty("Authorization", "Basic " + this.HttpCredentials);
      }
      
      urlConnection.connect();
      
      this.ISR                    = new InputStreamReader(urlConnection.getInputStream());
      
    } catch (IOException ex) {
      Logger.getLogger(SimpleJsonParser.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return this.fetchJsonContent();
  }  
  
  public List<T> fetchStringJsonContent(String jsonContent) {

    this.ResultList     = new ArrayList<>();
    this.resultElement  = this.jsonParser.parse(jsonContent);
    
    return this.generateResultList();
  }
  
    
  private List<T> fetchJsonContent() {
  
    this.ResultList     = new ArrayList<>();
    this.reader         = new JsonReader(this.ISR);
    this.resultElement  = this.jsonParser.parse(this.reader);
         
    return this.generateResultList();
  }
  

  private List<T> generateResultList() {
    
    if(this.resultElement.isJsonObject()) {
      
      this.resultObject = this.resultElement.getAsJsonObject();
      this.ResultList.add( (T) this.gsonInstance.fromJson(this.resultObject, this.ResultClassType) );
      
      return this.ResultList;
    }
    
    if(this.resultElement.isJsonArray()) {
      
      this.resultArray = this.resultElement.getAsJsonArray();
      
      for( JsonElement elem : this.resultArray ) {
        this.ResultList.add( (T) this.gsonInstance.fromJson(elem, this.ResultClassType) );
      }               
      
      return this.ResultList;
    }
    
    if(this.resultElement.isJsonPrimitive()) {
      
      this.resultString = this.resultElement.getAsString();
      this.ResultList.add( (T) this.gsonInstance.fromJson(this.resultString, this.ResultClassType));
      
      return this.ResultList;
    }
    
    return this.ResultList;    
  }  
  
}
