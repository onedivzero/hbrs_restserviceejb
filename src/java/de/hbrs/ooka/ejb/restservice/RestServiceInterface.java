package de.hbrs.ooka.ejb.restservice;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.representation.Form;
import javax.ejb.Stateful;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Micha
 */

@Stateful
public class RestServiceInterface {

  // NOTICE:
  // If you set jerseyResource.type not in a single-line call with jerseyResource.post(), 
  // the type does not get set and the JSON-Stuff will never be send ! Like this example:
  // jerseyResource.type("application/json");
  // jerseyResource.accept("application/json");
  // ClientResponse response = jerseyResource.post(ClientResponse.class, jsonString);
  // Correct way of doing this:
  // ClientResponse response = jerseyResource.accept("application/json").type("application/json").post(ClientResponse.class, jsonString);

  // Some MediaTypes:
  //jerseyResource.type(MediaType.APPLICATION_JSON_TYPE);
  //jerseyResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
  
  
  private Client jerseyClient;
  private WebResource jerseyResource;
  private ClientResponse jerseyResponse;
  private Form postForm;
  
  private String resourceURL      = "";
  private int responseState       = 0;
  private String responseString   = "";
  
  private static String JSON_TYPE = "application/json";
  
  private boolean debug_messages  = false;
  private boolean debug_data      = false;
  
  public RestServiceInterface() {
    
    this.jerseyClient = Client.create();
    this.postForm     = new Form();
  }

  public void setCredentials(String username, String password) {
    
    this.jerseyClient.addFilter(new HTTPBasicAuthFilter(username, password));
  }
  
  public void initResource(String Url) {
    
    this.resourceURL    = Url;
    this.jerseyResource = this.jerseyClient.resource(this.resourceURL);
    this.responseState  = 0;
    this.responseString = "";
  }
    
  public int getResponseState() {
    return responseState;
  }

  public String getResponseString() {
    return responseString;
  }

  // ----------------------------------------
  // Default REST-Methods for handling resources
  // ----------------------------------------

  public String getResource() {
    
    this.printDebugMessage("GET");
    
    this.jerseyResponse = this.jerseyResource.type(JSON_TYPE).get(ClientResponse.class);
    this.printResponseState();
    return this.responseString;    
  }
  
  
  public String postResource(String jsonContent) {
    
    // Notice: JSON-String muste be exactly in this format := "{\"key1\":\"value1\", \"key2\":\"value2\"}";
    this.printDebugMessage("POST", jsonContent);
    
    this.jerseyResponse = this.jerseyResource.accept(MediaType.APPLICATION_JSON).type(JSON_TYPE).post(ClientResponse.class, jsonContent);
    this.printResponseState();
    return this.responseString;    
  }
  
  public String putResource(String jsonContent) {

    this.printDebugMessage("PUT", jsonContent);

    this.jerseyResponse = this.jerseyResource.accept(MediaType.APPLICATION_JSON).type(JSON_TYPE).put(ClientResponse.class, jsonContent);
    this.printResponseState();
    return this.responseString;
  }

  public String deleteResource() {
    
    this.printDebugMessage("DELETE");

    this.jerseyResponse = this.jerseyResource.accept(MediaType.APPLICATION_JSON).type(JSON_TYPE).delete(ClientResponse.class);
    this.printResponseState();
    return this.responseString;
  }  
    
  // ----------------------------------------
  // Methods for handling multipart formdata
  // ----------------------------------------  
  
  public Form getPostForm() {
    return postForm;
  }

  public String postResourceFormEncoded(com.sun.jersey.api.representation.Form form) {
    
    this.postForm = form;
    return this.postResourceFormEncoded();    
  }
    
  public String postResourceFormEncoded() {

    this.printDebugMessage("POST", this.postForm.toString());
    
    this.jerseyResponse = this.jerseyResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, this.postForm);
    this.printResponseState();
    return this.responseString;
  }    
  
  
  // ----------------------------------------
  // Private helper methods
  // ----------------------------------------  
  
  private void printResponseState() {
    
    // Bug @ RestServiceInterface: Line 147 (Now Line 158)
    // Fix @ 2012.12.07 for NullPointerException caused by 'jerseyResponse.getClientResponseStatus()' 
    // If HTTP-Response fails for some reason (e.g. '422 Unprocessable Entity' which is a valid behaviour)
    // then getFamily() will be called on NULL, resulting in javax.ejb.EJBTransactionRolledbackException
    // on top of the whole stacktrace. This is a malfunction of Jersey, cause 'getStatus()' returns correctly
    // the statuscode '422' but 'getClientResponseStatus()' returns NULL instead of 'Unprocessable Entity'
    // FIXED: Added NullPointerCheck for involved Object 'com.sun.jersey.api.client.ClientResponse.Status'
    
    this.responseState  = this.jerseyResponse.getStatus();
    Status clientState  = this.jerseyResponse.getClientResponseStatus();    // ADDED
        
    // if(this.jerseyResponse.getClientResponseStatus().getFamily() == Response.Status.Family.SUCCESSFUL) { // REMOVED
    if(clientState != null && clientState.getFamily() == Response.Status.Family.SUCCESSFUL) {    
      
      System.out.println(this.getClass().getSimpleName() + "::Success " + this.responseState);
    }
    else {
      
      System.out.println(this.getClass().getSimpleName() + "::Error " + this.responseState);
    }
    
    this.responseString = (this.responseState != 204) ? this.jerseyResponse.getEntity(String.class) : "204 No Content";
    
    if(this.debug_data) { System.out.println(this.getClass().getSimpleName() + "::Result: " + this.responseString); }
  }
    
  
  private void printDebugMessage(String requestType) {
    this.printDebugMessage(requestType, null);
  }
  
  private void printDebugMessage(String requestType, String content) {
    if(this.debug_messages) { 
      System.out.println(this.getClass().getSimpleName() + ": " + requestType + "-Request to: " + this.resourceURL);
      if(this.debug_data && content != null) {
        System.out.println(this.getClass().getSimpleName() + ": Data:" + content + "\n");
      }
    }
  }  
  
  // ----------------------------------------
  // Public methods for qucikly dumping some state
  // ----------------------------------------    
  
  public void disableDebugMessages() {
    this.debug_messages = false;
  }
  
  public void enableDebugMessages() {
    this.debug_messages = true;
  }

  public void disableDebugData() {
    this.debug_data = false;
  }
  
  public void enableDebugData() {
    this.debug_data = true;
  }  
  
}
